package com.poquet;

import de.vandermeer.asciitable.AsciiTable;

public class Tablero {

    private Fichas[] arrayFichasRow1;
    private Fichas[] arrayFichasRow2;
    private Fichas[] arrayFichasRow3;
    private Fichas[] arrayFichasRow4;
    private Fichas[] arrayFichasRow5;
    private Fichas[] arrayFichasRow6;
    private Fichas[] arrayFichasCol1;
    private Fichas[] arrayFichasCol2;
    private Fichas[] arrayFichasCol3;
    private Fichas[] arrayFichasCol4;
    private Fichas[] arrayFichasCol5;
    private Fichas[] arrayFichasCol6;
    private Fichas[] arrayFichasCol7;


    public Tablero(){

        this.arrayFichasRow1 = new Fichas[7];
        this.arrayFichasRow2 = new Fichas[7];
        this.arrayFichasRow3 = new Fichas[7];
        this.arrayFichasRow4 = new Fichas[7];
        this.arrayFichasRow5 = new Fichas[7];
        this.arrayFichasRow6 = new Fichas[7];
        this.arrayFichasCol1 = new Fichas[6];
        this.arrayFichasCol2 = new Fichas[6];
        this.arrayFichasCol3 = new Fichas[6];
        this.arrayFichasCol4 = new Fichas[6];
        this.arrayFichasCol5 = new Fichas[6];
        this.arrayFichasCol6 = new Fichas[6];
        this.arrayFichasCol7 = new Fichas[6];


    }

    public AsciiTable crearTablero(){


        Fichas col1row1 = new Fichas(colorFicha.NONE,1,1);Fichas col2row1 = new Fichas(colorFicha.NONE,2,1);
        Fichas col3row1 = new Fichas(colorFicha.NONE,3,1);Fichas col4row1 = new Fichas(colorFicha.NONE,4,1);
        Fichas col5row1 = new Fichas(colorFicha.NONE,5,1);Fichas col6row1 = new Fichas(colorFicha.NONE,6,1);
        Fichas col7row1 = new Fichas(colorFicha.NONE,7,1);


        this.arrayFichasRow1[0]=col1row1;
        this.arrayFichasRow1[1]=col2row1;
        this.arrayFichasRow1[2]=col3row1;
        this.arrayFichasRow1[3]=col4row1;
        this.arrayFichasRow1[4]=col5row1;
        this.arrayFichasRow1[5]=col6row1;
        this.arrayFichasRow1[6]=col7row1;

        Fichas col1row2 = new Fichas(colorFicha.NONE,1,2);Fichas col2row2 = new Fichas(colorFicha.NONE,2,2);
        Fichas col3row2 = new Fichas(colorFicha.NONE,3,2);Fichas col4row2 = new Fichas(colorFicha.NONE,4,2);
        Fichas col5row2 = new Fichas(colorFicha.NONE,5,2);Fichas col6row2 = new Fichas(colorFicha.NONE,6,2);
        Fichas col7row2 = new Fichas(colorFicha.NONE,7,2);


        this.arrayFichasRow2[0]=col1row2;
        this.arrayFichasRow2[1]=col2row2;
        this.arrayFichasRow2[2]=col3row2;
        this.arrayFichasRow2[3]=col4row2;
        this.arrayFichasRow2[4]=col5row2;
        this.arrayFichasRow2[5]=col6row2;
        this.arrayFichasRow2[6]=col7row2;


        Fichas col1row3 = new Fichas(colorFicha.NONE,1,3);Fichas col2row3 = new Fichas(colorFicha.NONE,2,3);
        Fichas col3row3 = new Fichas(colorFicha.NONE,3,3);Fichas col4row3 = new Fichas(colorFicha.NONE,4,3);
        Fichas col5row3 = new Fichas(colorFicha.NONE,5,3);Fichas col6row3 = new Fichas(colorFicha.NONE,6,3);
        Fichas col7row3 = new Fichas(colorFicha.NONE,7,3);

        this.arrayFichasRow3[0]=col1row3;
        this.arrayFichasRow3[1]=col2row3;
        this.arrayFichasRow3[2]=col3row3;
        this.arrayFichasRow3[3]=col4row3;
        this.arrayFichasRow3[4]=col5row3;
        this.arrayFichasRow3[5]=col6row3;
        this.arrayFichasRow3[6]=col7row3;

        Fichas col1row4 = new Fichas(colorFicha.NONE,1,4);Fichas col2row4 = new Fichas(colorFicha.NONE,2,4);
        Fichas col3row4 = new Fichas(colorFicha.NONE,3,4);Fichas col4row4 = new Fichas(colorFicha.NONE,4,4);
        Fichas col5row4 = new Fichas(colorFicha.NONE,5,4);Fichas col6row4 = new Fichas(colorFicha.NONE,6,4);
        Fichas col7row4 = new Fichas(colorFicha.NONE,7,4);


        this.arrayFichasRow4[0]=col1row4;
        this.arrayFichasRow4[1]=col2row4;
        this.arrayFichasRow4[2]=col3row4;
        this.arrayFichasRow4[3]=col4row4;
        this.arrayFichasRow4[4]=col5row4;
        this.arrayFichasRow4[5]=col6row4;
        this.arrayFichasRow4[6]=col7row4;

        Fichas col1row5 = new Fichas(colorFicha.NONE,1,5);Fichas col2row5 = new Fichas(colorFicha.NONE,2,5);
        Fichas col3row5 = new Fichas(colorFicha.NONE,3,5);Fichas col4row5 = new Fichas(colorFicha.NONE,4,5);
        Fichas col5row5 = new Fichas(colorFicha.NONE,5,5);Fichas col6row5 = new Fichas(colorFicha.NONE,6,5);
        Fichas col7row5 = new Fichas(colorFicha.NONE,7,5);

        this.arrayFichasRow5[0]=col1row5;
        this.arrayFichasRow5[1]=col2row5;
        this.arrayFichasRow5[2]=col3row5;
        this.arrayFichasRow5[3]=col4row5;
        this.arrayFichasRow5[4]=col5row5;
        this.arrayFichasRow5[5]=col6row5;
        this.arrayFichasRow5[6]=col7row5;

        Fichas col1row6 = new Fichas(colorFicha.NONE,1,6);Fichas col2row6 = new Fichas(colorFicha.NONE,2,6);
        Fichas col3row6 = new Fichas(colorFicha.NONE,3,6);Fichas col4row6 = new Fichas(colorFicha.NONE,4,6);
        Fichas col5row6 = new Fichas(colorFicha.NONE,5,6);Fichas col6row6 = new Fichas(colorFicha.NONE,6,6);
        Fichas col7row6 = new Fichas(colorFicha.NONE,7,6);


        this.arrayFichasRow6[0]=col1row6;
        this.arrayFichasRow6[1]=col2row6;
        this.arrayFichasRow6[2]=col3row6;
        this.arrayFichasRow6[3]=col4row6;
        this.arrayFichasRow6[4]=col5row6;
        this.arrayFichasRow6[5]=col6row6;
        this.arrayFichasRow6[6]=col7row6;

        this.arrayFichasCol1[0]=col1row1;
        this.arrayFichasCol1[1]=col1row2;
        this.arrayFichasCol1[2]=col1row3;
        this.arrayFichasCol1[3]=col1row4;
        this.arrayFichasCol1[4]=col1row5;
        this.arrayFichasCol1[5]=col1row6;

        this.arrayFichasCol2[0]=col2row1;
        this.arrayFichasCol2[1]=col2row2;
        this.arrayFichasCol2[2]=col2row3;
        this.arrayFichasCol2[3]=col2row4;
        this.arrayFichasCol2[4]=col2row5;
        this.arrayFichasCol2[5]=col2row6;

        this.arrayFichasCol3[0]=col3row1;
        this.arrayFichasCol3[1]=col3row2;
        this.arrayFichasCol3[2]=col3row3;
        this.arrayFichasCol3[3]=col3row4;
        this.arrayFichasCol3[4]=col3row5;
        this.arrayFichasCol3[5]=col3row6;

        this.arrayFichasCol4[0]=col4row1;
        this.arrayFichasCol4[1]=col4row2;
        this.arrayFichasCol4[2]=col4row3;
        this.arrayFichasCol4[3]=col4row4;
        this.arrayFichasCol4[4]=col4row5;
        this.arrayFichasCol4[5]=col4row6;

        this.arrayFichasCol5[0]=col5row1;
        this.arrayFichasCol5[1]=col5row2;
        this.arrayFichasCol5[2]=col5row3;
        this.arrayFichasCol5[3]=col5row4;
        this.arrayFichasCol5[4]=col5row5;
        this.arrayFichasCol5[5]=col5row6;

        this.arrayFichasCol6[0]=col6row1;
        this.arrayFichasCol6[1]=col6row2;
        this.arrayFichasCol6[2]=col6row3;
        this.arrayFichasCol6[3]=col6row4;
        this.arrayFichasCol6[4]=col6row5;
        this.arrayFichasCol6[5]=col6row6;

        this.arrayFichasCol7[0]=col7row1;
        this.arrayFichasCol7[1]=col7row2;
        this.arrayFichasCol7[2]=col7row3;
        this.arrayFichasCol7[3]=col7row4;
        this.arrayFichasCol7[4]=col7row5;
        this.arrayFichasCol7[5]=col7row6;

        AsciiTable tablero = new AsciiTable();

        tablero.addRule();
        tablero.addRow(col1row1,col2row1,col3row1,col4row1,col5row1,col6row1,col7row1); //for dentro si i es igual a 5
        tablero.addRule();
        tablero.addRow(col1row2,col2row2,col3row2,col4row2,col5row2,col6row2,col7row2); //for dentro si i es igual a 4
        tablero.addRule();
        tablero.addRow(col1row3,col2row3,col3row3,col4row3,col5row3,col6row3,col7row3); //for dentro si i es igual a 4
        tablero.addRule();
        tablero.addRow(col1row4,col2row4,col3row4,col4row4,col5row4,col6row4,col7row4); //for dentro si i es igual a 4
        tablero.addRule();
        tablero.addRow(col1row5,col2row5,col3row5,col4row5,col5row5,col6row5,col7row5); //for dentro si i es igual a 4
        tablero.addRule();
        tablero.addRow(col1row6,col2row6,col3row6,col4row6,col5row6,col6row6,col7row6); //for dentro si i es igual a 4
        tablero.addRule();
        tablero.addRow("1","2","3","4","5","6","7");
        tablero.addRule();

        return tablero;

    }

    private void cambiarColorFicha(int column, int row, colorFicha colorFicha){

        column = column-1;

        if (row==1) {

            for (int i = 0; i < this.arrayFichasRow1.length; i++) {

                if (i == column) {

                    this.arrayFichasRow1[i].setColorFicha(colorFicha);

                }

            }
        }else if(row==2) {

            for (int i = 0; i < this.arrayFichasRow2.length; i++) {

                if (i==column){

                    this.arrayFichasRow2[i].setColorFicha(colorFicha);

                }

            }
        }else if (row==3) {
            for (int i = 0; i < this.arrayFichasRow3.length; i++) {

                if (i == column) {

                    this.arrayFichasRow3[i].setColorFicha(colorFicha);

                }

            }
        }else if (row==4) {
            for (int i = 0; i < this.arrayFichasRow4.length; i++) {

                if (i==column){

                    this.arrayFichasRow4[i].setColorFicha(colorFicha);

                }

            }
        }else if (row==5) {
            for (int i = 0; i < this.arrayFichasRow5.length; i++) {

                if (i==column){

                    this.arrayFichasRow5[i].setColorFicha(colorFicha);

                }

            }
        }else if (row==6) {
            for (int i = 0; i < this.arrayFichasRow6.length; i++) {

                if (i == column) {

                    this.arrayFichasRow6[i].setColorFicha(colorFicha);

                }

            }
        }
    }

    public void showTablero(AsciiTable tablero){

        System.out.println(tablero.render());

    }

    public void ponerFicha(int column, colorFicha colorFicha){

        if (column==1){

            int rowPos = getPosicionCaidaFicha(column);
            cambiarColorFicha(column,rowPos,colorFicha);

        }else if (column==2){

            int rowPos = getPosicionCaidaFicha(column);
            cambiarColorFicha(column,rowPos,colorFicha);

        }else if (column==3){

            int rowPos = getPosicionCaidaFicha(column);
            cambiarColorFicha(column,rowPos,colorFicha);

        }else if (column==4){

            int rowPos = getPosicionCaidaFicha(column);
            cambiarColorFicha(column,rowPos,colorFicha);

        }else if (column==5){

            int rowPos = getPosicionCaidaFicha(column);
            cambiarColorFicha(column,rowPos,colorFicha);

        }else if (column==6){

            int rowPos = getPosicionCaidaFicha(column);
            cambiarColorFicha(column,rowPos,colorFicha);

        }else if (column==7){

            int rowPos = getPosicionCaidaFicha(column);
            cambiarColorFicha(column,rowPos,colorFicha);

        }

    }

    public int getPosicionCaidaFicha(int column) {

        if (column == 1) {

            for (int i=this.arrayFichasCol1.length-1;i>=0;i--){

                if (arrayFichasCol1[i].getColorFicha()== colorFicha.NONE){

                    return i+1;

                }


            }

        }else if (column == 2) {

            for (int i=this.arrayFichasCol2.length-1;i>=0;i--){

                if (arrayFichasCol2[i].getColorFicha()== colorFicha.NONE){

                    return i+1;

                }


            }

        }else if (column == 3) {

            for (int i=this.arrayFichasCol3.length-1;i>=0;i--){

                if (arrayFichasCol3[i].getColorFicha()== colorFicha.NONE){

                    return i+1;

                }


            }

        }else if (column == 4) {

            for (int i=this.arrayFichasCol4.length-1;i>=0;i--){

                if (arrayFichasCol4[i].getColorFicha()== colorFicha.NONE){

                    return i+1;

                }


            }

        }else if (column == 5) {

            for (int i=this.arrayFichasCol5.length-1;i>=0;i--){

                if (arrayFichasCol5[i].getColorFicha()== colorFicha.NONE){

                    return i+1;

                }


            }

        }else if (column == 6) {

            for (int i=this.arrayFichasCol6.length-1;i>=0;i--){

                if (arrayFichasCol6[i].getColorFicha()== colorFicha.NONE){

                    return i+1;

                }


            }

        }else if (column == 7) {

            for (int i=this.arrayFichasCol7.length-1;i>=0;i--){

                if (arrayFichasCol7[i].getColorFicha()== colorFicha.NONE){

                    return i+1;

                }


            }

        }

        return column;
    }

    private colorFicha fromColRowGetCellColor(int column, int row){

        if (row==1){

            colorFicha color = this.arrayFichasRow1[column-1].getColorFicha();
            return color;

        }else if (row==2){

            colorFicha color = this.arrayFichasRow2[column-1].getColorFicha();
            return color;

        }else if (row==3){

            colorFicha color = this.arrayFichasRow3[column-1].getColorFicha();
            return color;

        }else if (row==4){

            colorFicha color = this.arrayFichasRow4[column-1].getColorFicha();
            return color;

        }else if (row==5){

            colorFicha color = this.arrayFichasRow5[column-1].getColorFicha();
            return color;

        }else if (row==6){

            colorFicha color = this.arrayFichasRow6[column-1].getColorFicha();
            return color;

        }

        return colorFicha.NONE;
    }

    private boolean comprobarLineaDiagonal(Fichas ficha) {

        int row = ficha.getRow();
        int column = ficha.getColumn();

        colorFicha colorFichaInicial = ficha.getColorFicha();

        if (colorFichaInicial != colorFicha.NONE) {

            if (column > 2 & column < 7 & row < 6 & row > 1) {

                if (fromColRowGetCellColor(column - 1, row - 1) == colorFichaInicial) {
                    if (fromColRowGetCellColor(column - 2, row - 2) == colorFichaInicial) {
                        if (fromColRowGetCellColor(column + 1, row + 1) == colorFichaInicial) {
                            return true;
                        }
                    }
                }
            }

            if (column < 6 & column > 1 & row < 6 & row > 2) {

                if (fromColRowGetCellColor(column + 1, row - 1) == colorFichaInicial) {
                    if (fromColRowGetCellColor(column + 2, row - 2) == colorFichaInicial) {
                        if (fromColRowGetCellColor(column - 1, row + 1) == colorFichaInicial) {
                            return true;
                        }
                    }
                }
            }

            if (column > 2 & column < 7 & row < 5 & row > 1) {

                if (fromColRowGetCellColor(column - 1, row + 1) == colorFichaInicial) {
                    if (fromColRowGetCellColor(column - 2, row + 2) == colorFichaInicial) {
                        if (fromColRowGetCellColor(column + 1, row - 1) == colorFichaInicial) {
                            return true;
                        }
                    }
                }
            }
            if (column < 6 & column > 1 & row < 5 & row > 1) {

                if (fromColRowGetCellColor(column + 1, row + 1) == colorFichaInicial) {
                    if (fromColRowGetCellColor(column + 2, row + 2) == colorFichaInicial) {
                        if (fromColRowGetCellColor(column - 1, row - 1) == colorFichaInicial) {
                            return true;
                        }
                    }

                }
            }

            if (column > 3) {

                if (fromColRowGetCellColor(column - 1, row - 1) == colorFichaInicial) {
                    if (fromColRowGetCellColor(column - 2, row - 2) == colorFichaInicial) {
                        if (fromColRowGetCellColor(column - 3, row - 3) == colorFichaInicial) {
                            return true;
                        }
                    }
                }
            }

            if (column < 5)

                if (fromColRowGetCellColor(column + 1, row - 1) == colorFichaInicial) {
                    if (fromColRowGetCellColor(column + 2, row - 2) == colorFichaInicial) {
                        if (fromColRowGetCellColor(column + 3, row - 3) == colorFichaInicial) {
                            return true;
                        }
                    }
                }

            if (column > 3) {

                if (fromColRowGetCellColor(column - 1, row + 1) == colorFichaInicial) {
                    if (fromColRowGetCellColor(column - 2, row + 2) == colorFichaInicial) {
                        if (fromColRowGetCellColor(column - 3, row + 3) == colorFichaInicial) {
                            return true;
                        }
                    }
                }
            }
            if (column < 5) {

                if (fromColRowGetCellColor(column + 1, row + 1) == colorFichaInicial) {
                    if (fromColRowGetCellColor(column + 2, row + 2) == colorFichaInicial) {
                        if (fromColRowGetCellColor(column + 3, row + 3) == colorFichaInicial) {
                            return true;
                        }
                    }

                }
            }
        }
        return false;
    }

    private boolean comprobarLineaVerticalHorizontal(Fichas ficha) {

        int row = ficha.getRow();
        int column = ficha.getColumn();

        colorFicha colorFichaInicial = ficha.getColorFicha();

        if (colorFichaInicial != colorFicha.NONE) {

            if (column < 7 & column > 2) {

                if (fromColRowGetCellColor(column - 1, row) == colorFichaInicial) {
                    if (fromColRowGetCellColor(column - 2, row) == colorFichaInicial) {
                        if (fromColRowGetCellColor(column + 1, row) == colorFichaInicial) {
                            return true;
                        }
                    }
                }
            }

            if (column < 6 & column > 1) {

                if (fromColRowGetCellColor(column + 1, row) == colorFichaInicial) {
                    if (fromColRowGetCellColor(column + 2, row) == colorFichaInicial) {
                        if (fromColRowGetCellColor(column - 1, row) == colorFichaInicial) {
                            return true;
                        }
                    }
                }
            }

            if (row > 2 & row < 6) {

                if (fromColRowGetCellColor(column, row - 1) == colorFichaInicial) {
                    if (fromColRowGetCellColor(column, row - 2) == colorFichaInicial) {
                        if (fromColRowGetCellColor(column, row + 1) == colorFichaInicial) {
                            return true;
                        }
                    }
                }
            }

            if (row > 1 & row < 5) {

                if (fromColRowGetCellColor(column, row + 1) == colorFichaInicial) {
                    if (fromColRowGetCellColor(column, row + 2) == colorFichaInicial) {
                        if (fromColRowGetCellColor(column, row + 3) == colorFichaInicial) {
                            return true;
                        }
                    }
                }
            }

            if (column > 3) {

                if (fromColRowGetCellColor(column - 1, row) == colorFichaInicial) {
                    if (fromColRowGetCellColor(column - 2, row) == colorFichaInicial) {
                        if (fromColRowGetCellColor(column - 3, row) == colorFichaInicial) {
                            return true;
                        }
                    }
                }
            }

            if (column < 5) {

                if (fromColRowGetCellColor(column + 1, row) == colorFichaInicial) {
                    if (fromColRowGetCellColor(column + 2, row) == colorFichaInicial) {
                        if (fromColRowGetCellColor(column + 3, row) == colorFichaInicial) {
                            return true;
                        }
                    }
                }
            }

            if (row > 3) {

                if (fromColRowGetCellColor(column, row - 1) == colorFichaInicial) {
                    if (fromColRowGetCellColor(column, row - 2) == colorFichaInicial) {
                        if (fromColRowGetCellColor(column, row - 3) == colorFichaInicial) {
                            return true;
                        }
                    }
                }
            }

            if (row < 4) {

                if (fromColRowGetCellColor(column, row + 1) == colorFichaInicial) {
                    if (fromColRowGetCellColor(column, row + 2) == colorFichaInicial) {
                        if (fromColRowGetCellColor(column, row + 3) == colorFichaInicial) {
                            return true;
                        }
                    }
                }
            }


        }
        return false;
    }

    public boolean comprobarPosibleGanador(Fichas ficha){

        boolean verticalHorizontal = comprobarLineaVerticalHorizontal(ficha);
        boolean diagonales = comprobarLineaDiagonal(ficha);


        if (verticalHorizontal==true | diagonales==true){

            return true;

        }else {

            return false;

        }


    }

}