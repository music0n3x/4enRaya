package com.poquet;

public class Fichas {

    private colorFicha colorFicha;
    private int column;
    private int row;

    public Fichas(colorFicha colorFicha, int column, int row){

        this.colorFicha=colorFicha;
        this.column=column;
        this.row=row;

    }

    public int getColumn() {
        return this.column;
    }

    public int getRow() {
        return this.row;
    }

    public void setColorFicha(colorFicha colorFicha) {
        this.colorFicha = colorFicha;
    }

    public colorFicha getColorFicha() {
        return colorFicha;
    }

    public String toString(){

        if (colorFicha==com.poquet.colorFicha.AMARILLO){
            return "amarillo";
        }else if (colorFicha==com.poquet.colorFicha.ROJO){
            return "rojo";
        }else {
            return "";
        }

    }




}
