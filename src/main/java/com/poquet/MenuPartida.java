package com.poquet;

import de.vandermeer.asciitable.AsciiTable;

import java.util.Random;
import java.util.Scanner;

public class MenuPartida {

    private Tablero tableroDeJuego;
    private colorFicha fichas1;
    private colorFicha fichas2;

    public MenuPartida(){

        this.tableroDeJuego = new Tablero();
        this.fichas1 = colorFicha.NONE;
        this.fichas2 = colorFicha.NONE;

    }

    public void comenzarPartida(){

        int eleccion;

        do {

            graficosGanador(algoritmoPartida());

            eleccion = askForRematchOrExit();

            if (eleccion == 1){

                System.out.println("Empezando nueva partida");

            }else if ( eleccion == 2 ){

                System.out.println("Saliendo del juego");

            }else {

                System.out.println("Introduce una opcion valida");
                eleccion = askForRematchOrExit();

            }

        }while (eleccion!=2);

    }

    public int askForRematchOrExit(){

        Scanner scanner = new Scanner(System.in);

        System.out.println("Partida finalizada, quieres volver a jugar");
        System.out.println("1 -> Jugar partida de nuevo");
        System.out.println("2 -> Salir");

        return scanner.nextInt();

    }

    public void graficosGanador(int jugador){

        if (jugador==1){

            System.out.println(" /               /     ///     /    /        ////     ///    ");
            System.out.println("  /             /             / /   /        /  //   ////    ");
            System.out.println("   /    / /    /       //     /  /  /        ////   // //    ");
            System.out.println("    /  /   /  /        //     /   / /        //        //    ");
            System.out.println("     /      /          //     /    /         //        //    ");

        }else if (jugador==2){

            System.out.println(" /               /     ///     /    /        ////     ////   ");
            System.out.println("  /             /             / /   /        /  //   // //    ");
            System.out.println("   /    / /    /       //     /  /  /        ////      //    ");
            System.out.println("    /  /   /  /        //     /   / /        //       //     ");
            System.out.println("     /      /          //     /    /         //      ////// ");

        }else if (jugador==3){

            System.out.println(" /               /     ///     /    /        /////     ////     //   //    ");
            System.out.println("  /             /             / /   /       //  //     /  //    //   //    ");
            System.out.println("   /    / /    /       //     /  /  /       //         ////     //   //    ");
            System.out.println("    /  /   /  /        //     /   / /       //  //     //       //   //    ");
            System.out.println("     /      /          //     /    /         /////     //        /////     ");

        }else {

            System.out.println("NO HAY GANADOR");

        }

    }

    public int algoritmoPartida(){

        boolean finPartida = false;


        System.out.println("Preparando partida 4 en raya...");
        preguntarColorFicha();
        AsciiTable asciiTablero = this.tableroDeJuego.crearTablero();
        int modoDeJuego = preguntarModoDeJuego();
        this.tableroDeJuego.showTablero(asciiTablero);

        if (modoDeJuego==2) {

            for (int i = 0; i < 42; i++) {

                finPartida = pedirFichaYcomprobarWinLoseJugador1(asciiTablero);
                if (finPartida == true) {
                    return 1;
                }
                finPartida = pedirFichaYcomprobarWinLoseJugador2(asciiTablero);
                if (finPartida == true) {
                    return 2;
                }
            }
        }else {

            for (int i = 0; i < 42; i++) {

                finPartida = pedirFichaYcomprobarWinLoseJugador1(asciiTablero);
                if (finPartida == true) {
                    return 1;
                }
                finPartida = getRandomFichaYcomprobarWinLoseJugador2CPU(asciiTablero);
                if (finPartida == true) {
                    return 3;
                }
            }
        }

        System.out.println("Partida acabada");

        return 3;

    }

    private int preguntarModoDeJuego(){

        Scanner scanner = new Scanner(System.in);
        int modoDeJuegoSeleccionado;



        System.out.println("Elige el modo de juego");
        System.out.println("1-> JUGADOR1 VS CPU");
        System.out.println("2-> JUGADOR1 VS JUGADOR2");

        modoDeJuegoSeleccionado = scanner.nextInt();

        if (modoDeJuegoSeleccionado==1 | modoDeJuegoSeleccionado==2){
            System.out.println("modo de juego elegido correctamente");
            return modoDeJuegoSeleccionado;
        }else {
            System.out.println("introduce una opcion valida");
            preguntarModoDeJuego();
        }


        return modoDeJuegoSeleccionado;

    }

    private boolean pedirFichaYcomprobarWinLoseJugador1(AsciiTable tablero){

        Fichas ultimaFicha;

        ultimaFicha = peticionMeterFichaJugador1(tablero);
        if (this.tableroDeJuego.comprobarPosibleGanador(ultimaFicha)==true){
            return true;
        }else {
            return false;
        }
    }

    private boolean pedirFichaYcomprobarWinLoseJugador2(AsciiTable tablero){

        Fichas ultimaFicha;

        ultimaFicha = peticionMeterFichaJugador2(tablero);
        if (this.tableroDeJuego.comprobarPosibleGanador(ultimaFicha)==true){
            return true;
        }else {
            return false;
        }
    }

    private boolean getRandomFichaYcomprobarWinLoseJugador2CPU(AsciiTable tablero){

        Fichas ultimaFicha = returnFichaRandomPos();
        this.tableroDeJuego.ponerFicha(ultimaFicha.getColumn(),this.fichas2);
        boolean winLose = this.tableroDeJuego.comprobarPosibleGanador(ultimaFicha);
        this.tableroDeJuego.showTablero(tablero);
        return winLose;

    }

    private Fichas returnFichaRandomPos(){

        Random r = new Random();
        int randomCol = r.nextInt((7 - 1) + 1) + 1;
        int row = this.tableroDeJuego.getPosicionCaidaFicha(randomCol);
        return new Fichas(this.fichas2,randomCol,row);
    }

    private void preguntarColorFicha(){

        Scanner scanner = new Scanner(System.in);

        System.out.println("Elige el color de tus fichas");
        System.out.println("1 -> rojas");
        System.out.println("2 -> amarillas");

        int eleccion = scanner.nextInt();

        if (eleccion == 1){

            this.fichas1 = colorFicha.ROJO;
            this.fichas2 = colorFicha.AMARILLO;

        }else if (eleccion == 2){

            this.fichas1 = colorFicha.AMARILLO;
            this.fichas2 = colorFicha.ROJO;

        }else {

            System.out.println("introduce una opcion valida");
            preguntarColorFicha();

        }

    }

    private Fichas peticionMeterFichaJugador1(AsciiTable tablero){

        Scanner scanner = new Scanner(System.in);

        System.out.println("Jugador 1, "+this.fichas1);
        System.out.println("En que columna quieres poner una ficha");

        String eleccion = scanner.next();

        if (eleccion.matches("[0-7]")){

            Integer eleccionInt = Integer.parseInt(eleccion);

            int row = this.tableroDeJuego.getPosicionCaidaFicha(eleccionInt);

            this.tableroDeJuego.ponerFicha(eleccionInt,this.fichas1);

            this.tableroDeJuego.showTablero(tablero);

            return new Fichas(this.fichas1,eleccionInt,row);

        }else {

            System.out.println("Introduce una opcion valida");
            peticionMeterFichaJugador1(tablero);
            return new Fichas(colorFicha.NONE,6,4);
        }

    }

    private Fichas peticionMeterFichaJugador2(AsciiTable tablero){

        Scanner scanner = new Scanner(System.in);

        System.out.println("Jugador 2, "+this.fichas2);
        System.out.println("En que columna quieres poner una ficha");

        String eleccion = scanner.next();

        if (eleccion.matches("[0-9]")){

            Integer eleccionInt = Integer.parseInt(eleccion);

            int row = this.tableroDeJuego.getPosicionCaidaFicha(eleccionInt);

            this.tableroDeJuego.ponerFicha(eleccionInt,this.fichas2);

            this.tableroDeJuego.showTablero(tablero);

            return new Fichas(this.fichas2,eleccionInt,row);


        }else {

            System.out.println("Introduce una opcion valida");
            peticionMeterFichaJugador2(tablero);
            return new Fichas(colorFicha.NONE,6,4);
        }


    }

}
